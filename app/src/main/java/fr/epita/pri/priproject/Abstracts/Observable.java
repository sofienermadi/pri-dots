package fr.epita.pri.priproject.Abstracts;

import java.util.List;

/**
 * Created by rmadi on 17/02/2018.
 */

public interface Observable
{
    List<Observer> observers = null ;
    void notifierObservers();
    public void addObserver(Observer observateur);
    public void detachObser(Observer observateur);
}
