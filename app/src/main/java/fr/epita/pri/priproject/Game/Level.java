package fr.epita.pri.priproject.Game;

import fr.epita.pri.priproject.Abstracts.Observable;
import fr.epita.pri.priproject.Abstracts.Observer;

/**
 * Created by sofie on 17/02/2018.
 */

public class Level implements Observable {
    @Override
    public void notifierObservers() {

    }

    @Override
    public void addObserver(Observer observateur) {

    }

    @Override
    public void detachObser(Observer observateur) {

    }
}
