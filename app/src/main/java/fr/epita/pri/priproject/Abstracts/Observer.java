package fr.epita.pri.priproject.Abstracts;

/**
 * Created by rmadi on 17/02/2018.
 */

public interface Observer
{
    public void notifyObserver();
}
