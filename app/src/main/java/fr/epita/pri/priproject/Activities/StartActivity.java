package fr.epita.pri.priproject.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import fr.epita.pri.priproject.R;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
    }
}
